var express = require('express');  // módulo express
var app = express();		   // objeto express
var server = require('http').Server(app);
var socketio = require('socket.io')(server);

var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;


function proxy(id, servidor, nick, canal, socket) {
// 	var cache = []; // cache de mensagens
	var ws = socket;

	var irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});

	// Tratativa de mensagem recebida no Proxy enviada pelo IRC Server
	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    ws.emit(	"message", {"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );
	});
	
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	irc_client.addListener('mode', function(message) {
	    console.log('mode: ', message);
	});
	
	irc_client.addListener('motd', function(motd){
		ws.emit("message", {"timestamp":Date.now(), 
							"msg":motd} );
	});
	
	irc_client.addListener('join', function(channel, nick, message){
		ws.emit("join", {"timestamp":Date.now(), 
							"channel":channel,
							"nick": nick});
	});
	
	irc_client.addListener('names', function(channel, nicks){
		var lista = channel+JSON.stringify(nicks);
		console.log('listagem de nicks do canal: '+lista);
		ws.emit("names", {"timestamp":Date.now(), 
							"channel":channel,
							"nicks": nicks});
	});
	
	
	proxies[id] = { "ws":socket, "irc_client":irc_client  };

	return proxies[id];
}

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	proxy_id++;
	
	socketio.on("connection", function (socket) {
		console.log("conectou: ", "server: " + req.cookies.servidor + ", nick: " + req.cookies.nick + ", canal: " + req.cookies.canal);
		
		var p =	proxy(	proxy_id,
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal,
				socket);
		
		// Tratativa da mensagem recebida no Proxy enviada pelo web socket, recebe o iosocket.send
		socket.on("message", function (message, arg) {
				var splited = message.split(' ');
				if(message == '/motd'){
					proxies[proxy_id].irc_client.send('motd');
				}
				else if(splited[0] == '/part'){
					proxies[proxy_id].irc_client.send('part', splited[1]);
					var proxyArray = Object.values(proxies);
					var indice = proxyArray.indexOf(proxy_id);
					proxyArray.splice(indice);
					res.sendFile(path.join(__dirname, '/login.html'));
				}
				else{
					console.log("mensagem:"+message);
					proxies[proxy_id].irc_client.say(req.cookies.canal, message);
				}
		});
		
	});
	res.cookie('id', proxy_id);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});

app.post('/gravar_mensagem', function (req, res) {
  // proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  if(req.body.msg == '/motd'){
	  irc_client.send("motd");
  }
  else{
	irc_client.say(req.cookies.canal, irc_client.opt.channels[0], req.body.msg );  
  }  
  res.end();
});

app.get('/mode/:usuario/:args', function (req, res){
  var usuario = req.params.usuario;
  var args = req.params.args;
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", usuario, args);
  res.send(retorno);
});
app.get('/mode/', function (req, res){
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", req.cookies.nick);
  res.send(retorno);
});


app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);	
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

server.listen(3000, function () {				
  console.log('Example app listening on port 3000!');	
});
