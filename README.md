# Projeto 2

Protótipo de cliente web para servidor IRC da disciplina de aplicações/sistemas distribuídos

### Lista de membros do grupo
**Nome** | **Função** |
:---:|:---------:
Ygor Santos| Líder e Desenvolvedor |
Rhandy Mendes Ferreira| Desenvolvedor |
Victor Murilo| Documentador |
Pedro Andrade| Documentador |

## Introdução

Este repositório foi criado para que seja feito o segundo projeto do grupo 1, da disciplina Aplicações Distríbuidas da Universidade Federal de Goiás, ministrado pelo professor Marcelo Akira.

## Requisitos

Para que o projeto funcione, é necessário instalar os módulos do Nodejs e utilizar um navegador qualquer (Chrome, Firefox, Iexplorer, etc).

## Funcionamento

Através do terminal, deve ser utilizado o comando node app.js para deixar o projeto "online".
Por padrão, a porta para acessar é a 3000.


## Execução dos arquivos

Primeira tentativa de execução foi no arquivo teste-irc.js.

Foi necessário atualizar até a versão mais recente do nodejs(aparentemente 4.x pra cima), senão aparece a seguinte mensagem de erro:

**Tipo de erro** | **Mensagem**
:---:|:---------:
SyntaxError:| Use of const in strict mode.|

Assim que resolvido, utilizando os comandos **npm install n -g** e **n latest**, demos prosseguimento e a execução de *teste-irc.js* foi um sucesso.

Já a segunda tentativa de execução, do app.js que é um arquivo que exibe o horário atual da máquina,
tivemos apenas que instalar um módulo requerido chamado *body-parser*, que logo após isso tudo ocorreu devidamente.